<h1>Description</h1>

The CalendarControl is a QControl and requires the QControl Toolkit, <a href="http://bit.ly/QControlNITools">http://bit.ly/QControlNITools.</a>.  It inherits from and extends the Cluster control.  It implements multiple buttons, lists, etc. to implement a Calandar.  It has a properties to customize:
- Start Day of Week
- Colors of different components
- Toggle visibility of horizontal and verticle lines

<h1>LabVIEW Version</h1>

This code is currently published in LabVIEW 2018

<h1>Build Instructions</h1>

The <b>Calendar.lvclass</b> and all its members can be distributed as a Class Library and built into the using application.

<h1>Installation Guide</h1>

The <b>Calendar.lvclass</b> and all its members can be distributed as a Class Library and built into the using application.

<h1>Execution</h1>
See documentation distributed with the QControl Toolkit.

<h1>Support</h1>
Submit Issues or Merge Requests through GitLab.